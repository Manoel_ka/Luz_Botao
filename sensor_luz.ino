int ldrPin=0;
int ldrValor =0;
int lumAmb =  0;

void setup() {
  pinMode(12,OUTPUT);
  pinMode(3,INPUT);
  digitalWrite(12, LOW);
  Serial.begin(9600);
}

void loop() {

  ldrValor=analogRead(ldrPin);//essa função retornara o valor que indicara a ocorrência de luz
  
  //Na função abaixo irar tratar ldrValor e determinara quando ligar
  //ou desligar o LED e retornara o estado do ambiente
  lumAmb=sensorLuz(ldrValor);

  //Se o botão for acionado essa função irar desligar ou ligar,
  //por um tempo determinado o LED, de acordo com  o estado de lumAmb.  
  acionaBotao(lumAmb);
  
  Serial.println(ldrValor);
  delay(100);
}


void acionaBotao(int lumAmb)
{
  if (digitalRead(3)==1)
  {
    if(lumAmb==0)
      digitalWrite(12, HIGH);
    else  
      digitalWrite(12, LOW);

    delay(5000);
  }
}

int sensorLuz(int ldrValor)
{
  int lumAmb;
  if(ldrValor>=960){
    lumAmb=1;
    digitalWrite(12,HIGH);
  }
  else{
    digitalWrite(12,LOW);
    lumAmb=0;  
  }
  return lumAmb;
}
